const toBanner = () => {
  stopSound('mainSound')
  $('#main').html(banner())
  let countBannerDelay = 0
  let bannerDelay = setInterval(() => {
    countBannerDelay++
    if (countBannerDelay == 1) {
      toWaitClick()
      clearInterval(bannerDelay)
    }
  }, 5000)
}

const toWaitClick = () => {
  $('#main').html(waitClick())
}

const toMain = () => {
  $('#main').html(main())
  stopSound('mainSound')
  playSound('mainSound')
}


const toHome = () => {
  $('#main').html(home())
  playSound('mainSound')
}

const toGuideP1 = () => {
  $('#main').html(guideP1())
}

const toGuideP2 = () => {
  $('#main').html(guideP2())
}

const toGuideP3 = () => {
  $('#main').html(guideP3())
}

const toGuideP4 = () => {
  $('#main').html(guideP4())
}

const toGuideP5 = () => {
  $('#main').html(guideP5())
}

const toGuideP6 = () => {
  $('#main').html(guideP6())
}

const toHighscore = () => {
  $('#main').html(highscore())
}

const toMode = () => {
  $('#main').html(mode())
  stopSound('arenaV2Sound')
  stopSound('arenaSound')
  playSound('mainSound')
}

const toStore = () => { 
  $('#main').html(store())
}

const toShop = () => {
  $('#main').html(shop())
}

const toSurvive = () => {
  playMode = 2
  $('#main').html(survive())
  playSurvive()
}

const toCoins = () => {
  playMode = 1
  $('#main').html(coins())
  playCoins()
}

const toEndSurvive = () => {
  $('#main').append(endSurvive())
  $('#end-modal').delay(1000).fadeIn('slow')
}

const toEndCoins = () => {
  $('#main').append(endCoins(bossHp))
  $('#end-modal').delay(1000).fadeIn('slow')
}



