const usePlane = (index) => {
  if (!myff.myPlanes[index].isUse) {
    myff.myPlanes.forEach(plane => {
      if (plane.isUse) {
        plane.isUse = false
        plane.card = plane.img
      }      
    })

    myff.myPlanes[index].isUse = true
    myff.myPlanes[index].card = 'use-' + myff.myPlanes[index].img
    myff.currentPlane = myff.myPlanes[index]
  }

  //update
  let el = ''
  myff.myPlanes.forEach((plane, index) => {
    el += '<img src="./assets/img/card/'+ plane.card + '" id="planeBtn" onclick="usePlane(' + index + ')"/>'
  });

  saveMyff()
  $('#planesBox').html(el)
}

const confirmToBuy = (index) => {
  if (!myff.shopPlanes[index].isBought) {
    if (myff.shopPlanes[index].price > myff.userCoin ) {
      $('#nocoinsModal-' + index).show()
    } else {
      $('#buyModal-' + index).show()
    }
  }
}

const confirmResetData = () => {
  $('#confirmResetModal').show()
}

const closeModal = (modal) => {
  $('#'+ modal).hide()
}

const buyPlane = (index) => {
  $('#buyModal-' + index).hide()
  myff.userCoin -= myff.shopPlanes[index].price
  //update myff.userCoin
  $('#coin').text(myff.userCoin)

  myff.shopPlanes[index].isBought = true
  myff.shopPlanes[index].card = 'buy-' + myff.shopPlanes[index].img

  let tempPlane = {...myff.shopPlanes[index]}
  tempPlane.img = tempPlane.img.replace(/shop-/,'')
  tempPlane.card = tempPlane.img
  tempPlane.isUse = false
  delete tempPlane.isBought
  myff.myPlanes.splice(tempPlane._id, 0, tempPlane)

  //update
  let el = ''
  myff.shopPlanes.forEach((plane, index) => {
    el += '<img src="./assets/img/card/'+ plane.card + '" id="planeBtn" onclick="confirmToBuy(' + index + ')"/>'
  });
    
  $('#planesBox').html(el)

  el = ''
  myff.shopPlanes.forEach((plane, index) => {
    el += '<div id="buyModal-'+ index +'" class="modal"><div class="modal-content-buy"><div class="modal-footer-game"><img src="./assets/img/button/yes.png" onclick="buyPlane('+ index +')" id="modalBtn" onmouseover="playSound(\'mouseSound\')"><img src="./assets/img/button/no.png" onclick="closeModal(\'buyModal-'+ index +'\')" id="modalBtn" onmouseover="playSound(\'mouseSound\')"></div></div></div>'

    el += '<div id="nocoinsModal-'+ index +'" class="modal"><div class="modal-content-nocoins"><div class="modal-footer-game"><img src="./assets/img/button/no.png" onclick="closeModal(\'nocoinsModal-'+ index +'\')" id="modalBtn" onmouseover="playSound(\'mouseSound\')"></div></div></div>'
  });
  
  saveMyff()
  $('#main').append(el)
}


const playSound = (id) => {
  $('#' + id).trigger('play');
}

const pauseSound = (id) => {
  $('#' + id).trigger('pause');
}

const stopSound = (id) => {
  $('#' + id).trigger('pause')
  document.getElementById(id).currentTime = 0
}

const saveMyff = () => {
  window.localStorage.setItem('myff', JSON.stringify(myff))
}

const resetData = () => {
  window.localStorage.clear()
  initMyff()
  saveMyff()
  $('#confirmResetModal').hide()
  toBanner()
}

const initMyff = () => {
  myff.shopPlanes = [
    {
      _id: 1,
      name: 'Jason',
      img: 'shop-scrim.png',
      card: 'shop-scrim.png',
      isBought: false,
      price: 15,
      imgLeft: './assets/img/plane/scrimplaneLeft.png',
      imgRight: './assets/img/plane/scrimplaneRight.png',
      imgLeftSpeed: './assets/img/plane/scrimplaneLeftSpd.png',
      imgRightSpeed: './assets/img/plane/scrimplaneRightSpd.png'
    },
    {
      _id: 2,
      name: 'The Flash',
      img: 'shop-flash.png',
      card: 'shop-flash.png',
      isBought: false,
      price: 25,
      imgLeft: './assets/img/plane/flashLeft.png',
      imgRight: './assets/img/plane/flashRight.png',
      imgLeftSpeed: './assets/img/plane/flashLeftSpd.png',
      imgRightSpeed: './assets/img/plane/flashRightSpd.png'
    },
    {
      _id: 3,
      name: 'UFO',
      img: 'shop-ufo.png',
      card: 'shop-ufo.png',
      isBought: false,
      price: 35,
      imgLeft: './assets/img/plane/spaceshipLeft.png',
      imgRight: './assets/img/plane/spaceshipRight.png',
      imgLeftSpeed: './assets/img/plane/spaceshipLeftSpd.png',
      imgRightSpeed: './assets/img/plane/spaceshipRightSpd.png'
    }
  ]
  myff.myPlanes = [
    {
      _id: 0,
      name: 'Plane',
      img: 'plane.png',
      card: 'use-plane.png',
      isUse: true,
      imgLeft: './assets/img/plane/FlyLeft01.png',
      imgRight: './assets/img/plane/FlyRight01.png',
      imgLeftSpeed: './assets/img/plane/FlyLeft02.png',
      imgRightSpeed: './assets/img/plane/FlyRight02.png'
    }
  ]

  myff.userCoin = 0
  myff.currentPlane = myff.myPlanes[0];
  myff.maxScore = 0
  myff.timePlaySurvive = 0
}

function formatCoins (value) {
  if (value > 999999) return '+999,999'
  return toCommas(value)
}

function toCommas(value) {
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}