const banner = () => {
  return '<div id="blackBox"><img id="banner" src="./assets/img/optional/banner.png"></div>'
}

const waitClick = () => {
  return '<div id="blackBox"><img id="clickBlink" src="./assets/img/optional/clickBlink.png" onclick="toMain()" ></div>'
}

const headBar = (to) => {
  return '<div class="justify-content-between" id="headBar"><div id="coinBox"><span id="coin">' + formatCoins(myff.userCoin) + '</span></div><img src="./assets/img/button/exit.png" id="exitBtn" onclick="' + to + '()" onmouseover="playSound(\'mouseSound\')"></div>' 
}

const main = () => {
  let el = '<div id="titleBox"><img src="./assets/img/optional/title.png" id="title"></div><div id="playBtnBox"><img src="./assets/img/button/play.png" id="playBtn" onclick="toHome()" onmouseover="playSound(\'mouseSound\')"></div><div id="footerBox"><img src="./assets/img/button/resetData.png" id="resetDataBtn" onclick="confirmResetData()" onmouseover="playSound(\'mouseSound\')"></div><div id="confirmResetModal" class="modal"><div class="modal-content-reset"><div class="modal-footer-game"><img src="./assets/img/button/yes.png" onclick="resetData()" id="modalBtn" onmouseover="playSound(\'mouseSound\')"><img src="./assets/img/button/no.png" onclick="closeModal(\'confirmResetModal\')" id="modalBtn" onmouseover="playSound(\'mouseSound\')"></div></div></div>'
  return cloud()+ el + sound('mouse')
}

const home = () => {
  let el = '<div id="menuBar"><img src="./assets/img/button/guide.png" id="menuBtn" onclick="toGuideP1()" onmouseover="playSound(\'mouseSound\')"><img src="./assets/img/button/mission.png" id="menuBtn" onclick="toHighscore()" onmouseover="playSound(\'mouseSound\')"><img src="./assets/img/button/shop.png" id="menuBtn" onclick="toShop()" onmouseover="playSound(\'mouseSound\')"><img src="./assets/img/button/store.png" id="menuBtn" onclick="toStore()" onmouseover="playSound(\'mouseSound\')"><img src="./assets/img/button/travelV2.png" id="travelBtn" onclick="toMode()" onmouseover="playSound(\'mouseSound\')"></div>'
  return cloud()+ headBar('toMain') + showPlane() + el + sound('mouse')
}

const highscore = () => {
  let el = '<div id="topicBox"><img src="./assets/img/optional/highscore.png"></div><div id="guideBg"></div>'
  if (myff.timePlaySurvive == 0 && myff.maxScore == 0) {
    el += '<div id="highscoreBox"><h2 style="margin-top: 150px; width:600px; text-align:center">You have never</h2><h2 style="width:600px; text-align:center">encountered a survive mode.</h2></div>'
  } else {
    el += '<div id="highscoreBox"><div class="highscore-item" style="margin-top: 80px; margin-bottom: 30px;"><h2>Survive Mode</h2></div><div class="highscore-item" ><img src="./assets/img/optional/trophy.png" width="120px" height="130px" style="margin-right: 20px"><span style="font-size: 150px; line-height: 150px">' + myff.maxScore + '</span><span style="font-size: 40px; margin-left: 20px">points</span></div></div>'
  }

  return headBar('toHome') + el + sound('mouse')
}

const guideP1 = () => {
  let el = '<div id="topicBox"><img src="./assets/img/optional/guide.png"></div><div id="guideBg"></div><img src="./assets/img/optional/guideP1.png" id="guideImg" ><div id="guideBtnBox" class="justify-content-end"><img src="./assets/img/button/next.png" id="guideBtn" onmouseover="playSound(\'mouseSound\')" onclick="toGuideP2()"></div>'
  return headBar('toHome') + el + sound('mouse')
}

const guideP2 = () => {
  let el = '<div id="topicBox"><img src="./assets/img/optional/guide.png"></div><div id="guideBg"></div><img src="./assets/img/optional/guideP2.png" id="guideImg" ><div id="guideBtnBox" class="justify-content-between"><img src="./assets/img/button/back.png" id="guideBtn" onmouseover="playSound(\'mouseSound\')" onclick="toGuideP1()"><img src="./assets/img/button/next.png" id="guideBtn" onmouseover="playSound(\'mouseSound\')" onclick="toGuideP3()"></div>'
  return headBar('toHome') + el + sound('mouse')
}

const guideP3 = () => {
  let el = '<div id="topicBox"><img src="./assets/img/optional/guide.png"></div><div id="guideBg"></div><img src="./assets/img/optional/guideP3.png" id="guideImg" ><div id="guideBtnBox" class="justify-content-between"><img src="./assets/img/button/back.png" id="guideBtn" onmouseover="playSound(\'mouseSound\')" onclick="toGuideP2()"><img src="./assets/img/button/next.png" id="guideBtn" onmouseover="playSound(\'mouseSound\')" onclick="toGuideP4()"></div>'
  return headBar('toHome') + el + sound('mouse')
}

const guideP4 = () => {
  let el = '<div id="topicBox"><img src="./assets/img/optional/guide.png"></div><div id="guideBg"></div><img src="./assets/img/optional/guideP4.png" id="guideImg" ><div id="guideBtnBox" class="justify-content-between"><img src="./assets/img/button/back.png" id="guideBtn" onmouseover="playSound(\'mouseSound\')" onclick="toGuideP3()"><img src="./assets/img/button/next.png" id="guideBtn" onmouseover="playSound(\'mouseSound\')" onclick="toGuideP5()"></div>'
  return headBar('toHome') + el + sound('mouse')
}

const guideP5 = () => {
  let el = '<div id="topicBox"><img src="./assets/img/optional/guide.png"></div><div id="guideBg"></div><img src="./assets/img/optional/guideP5.png" id="guideImg" ><div id="guideBtnBox" class="justify-content-between"><img src="./assets/img/button/back.png" id="guideBtn" onmouseover="playSound(\'mouseSound\')" onclick="toGuideP4()"><img src="./assets/img/button/next.png" id="guideBtn" onmouseover="playSound(\'mouseSound\')" onclick="toGuideP6()"></div>'
  return headBar('toHome') + el + sound('mouse')
}

const guideP6 = () => {
  let el = '<div id="topicBox"><img src="./assets/img/optional/guide.png"></div><div id="guideBg"></div><img src="./assets/img/optional/guideP6.png" id="guideImg" ><div id="guideBtnBox" class="justify-content-start"><img src="./assets/img/button/back.png" id="guideBtn" onmouseover="playSound(\'mouseSound\')" onclick="toGuideP5()"></div>'
  return headBar('toHome') + el + sound('mouse')
}

const mode = () => {
  let el = '<div id="topicBox"><img src="./assets/img/optional/mode.png"></div><div id="modeBox"><img src="./assets/img/button/coinMode.png" id="modeBtn" onclick="toCoins()" onmouseover="playSound(\'mouseSound\')"><img src="./assets/img/button/surviveMode.png" id="modeBtn" onclick="toSurvive()" onmouseover="playSound(\'mouseSound\')"></div>'
  return headBar('toHome') + el + sound('mouse')
}

const store = () => {
  let el = '<div id="topicBox"><img src="./assets/img/optional/inventory.png"></div><div id="planesBox">'
  myff.myPlanes.forEach((plane, index) => {
    el += '<img src="./assets/img/card/'+ plane.card + '" id="planeBtn" onclick="usePlane(' + index + ')"/ onmouseover="playSound(\'mouseSound\')">'
  });
  el += '</div>'
  return  headBar('toHome') + el + sound('mouse')
}

const shop = () => {
  let el = '<div id="topicBox"><img src="./assets/img/optional/shop.png"></div><div id="planesBox">'
  myff.shopPlanes.forEach((plane, index) => {
    el += '<img src="./assets/img/card/'+ plane.card + '" id="planeBtn" onclick="confirmToBuy(' + index + ')"/ onmouseover="playSound(\'mouseSound\')">'
  });
  el += '</div>'
  myff.shopPlanes.forEach((plane, index) => {
    el += '<div id="buyModal-'+ index +'" class="modal"><div class="modal-content-buy"><div class="modal-footer-game"><img src="./assets/img/button/yes.png" onclick="buyPlane('+ index +')" id="modalBtn" onmouseover="playSound(\'mouseSound\')"><img src="./assets/img/button/no.png" onclick="closeModal(\'buyModal-'+ index +'\')" id="modalBtn" onmouseover="playSound(\'mouseSound\')"></div></div></div>'

    el += '<div id="nocoinsModal-'+ index +'" class="modal"><div class="modal-content-nocoins"><div class="modal-footer-game"><img src="./assets/img/button/no.png" onclick="closeModal(\'nocoinsModal-'+ index +'\')" id="modalBtn" onmouseover="playSound(\'mouseSound\')"></div></div></div>'
  });
  el += '</div>'
  return  headBar('toHome') + el + sound('mouse')
}

const survive = () => {
  let el = '<div class="justify-content-between" id="headBarV2"><div id="scoreBox"><span id="score">' + score + '</span></div><span class="time"><h1 id="time">01:00</h1></span></div>'
  return el
}

const coins = () => {
  let el = '<div class="justify-content-between" id="headBarV2"><div id="coinBox"><span id="coin">' + formatCoins(coinItem) + '</span></div><div id="bossBox"><span id="bossHp">' + bossHp + '</span></div></div>'
  return el
}

const endSurvive = () => {
  let el = '<div class="modal" id="end-modal"><div id="endSurvive"><div id="end-text-box"><img src="./assets/img/optional/score.png"><span>' + score + '</span></div><div id="end-btn-box"><img src="./assets/img/button/exit.png" id="end-btn" onclick="toMode()" onmouseover="playSound(\'mouseSound\')"></div></div></div>'
  return el + sound('mouse')
}

const endCoins = (hp) => {
  let el
  if (hp == 0) {
    el = '<div class="modal" id="end-modal"><div id="endCoinsP"><div id="end-text-box"><img src="./assets/img/optional/coin.png"><span>' + formatCoins(coinItem) + '</span></div><div id="end-btn-box"><img src="./assets/img/button/exit.png" id="end-btn" onclick="toMode()" onmouseover="playSound(\'mouseSound\')"></div></div></div>'
  } else {
    el = '<div class="modal" id="end-modal"><div id="endCoinsF"><div id="end-text-box"><img src="./assets/img/optional/coin.png"><span>' + formatCoins(( coinItem - (Math.ceil(coinItem/2)) )) + '</span></div><div id="end-btn-box"><img src="./assets/img/button/exit.png" id="end-btn" onclick="toMode()" onmouseover="playSound(\'mouseSound\')"></div></div></div>'
  }
  return el + sound('mouse')
}


const sound = (file, loop = false) => {
  if (loop) {
    return '<audio id="' + file + 'Sound" controls preload="none" loop style="display:none;"><source src="./assets/sound/'+ file +'.mp3" type="audio/mpeg"></audio>'
  } else {
    return '<audio id="' + file + 'Sound" controls preload="none" style="display:none;"><source src="./assets/sound/'+ file +'.mp3" type="audio/mpeg"></audio>'
  }
}

const showPlane = () => {
  let el = '<div id="showPlaneBox"><img id="cardImg" src="./assets/img/card/'+ myff.currentPlane.img +'" ><img id="charImg" src="' + myff.currentPlane.imgRight +'"></div>'
  return el
}

const cloud = () => {
  let el = '<div id="background-wrap"><div class="x1"><div class="cloud"></div></div><div class="x2"><div class="cloud"></div></div><div class="x3"><div class="cloud"></div></div><div class="x4"><div class="cloud"></div></div><div class="x5"><div class="cloud"></div></div></div>'
  return el
}
