/** ----- Define Variable for All ----- */

/** background */
var backgroundImgList = ['./assets/img/bg/v1.png','./assets/img/bg/v2.jpg']
var background

/** scene */
var scene

/** plane */
var plane

/** random for spawn obj */
var spawnRand

/** array for keep obj */
var fruits = []
var missiles = []
var clocks = []

/** bool game over */
var boolGameOver = false

/** btoom */
var btoom 

/** ----- Define Variable for Survive ----- */

/** score  */
var score = 0

/** boost speed bottom */
var bottom = null
var boolUseBottom = false
var boolBottom = false

/** timer interval for time count down */
var timer = 0,
  minutes,
  seconds
var countTimer = 0
var intervalID

/** timer interval for increase speed of missile */
var boostSpeed = 0
var boostIntervalID

/** timer interval for count down item (boost speed) */
var countBottom = 0
var bottomIntervalID

/** for chane Speed of plane  */
var distChangePlaneX

/** bool and count for show increase time 5 seconds ( +5 ) */
var boolAddFive = false
var countAddFive = 0

/** sound */
var themeSong

/** ----- Define Variable for Coins ----- */
var coinItem = 0
var bossHp = 50

var coinsList = []


const playSurvive = () => {
  /** random map */
  let bgRand = Math.floor(Math.random()*2)

  /** create scene */
  scene = new Scene('main')
  scene.setSize(900, 700)
  
  /** create plane  */
  plane = new Sprite(scene, myff.currentPlane.imgLeft, 150, 100)
  plane.setPosition(scene.width / 2, scene.height - 107)
  plane.setSpeed(0)
  
  /** create background in scene */
  background = new Sprite(scene,backgroundImgList[bgRand], 900 + 500, 700 + 300)
  background.setSpeed(0)

  /** create btoom */
  btoom = new Sprite(scene, './assets/img/bomb/btoom.png', 300, 300)

  /** reset value of var for init process */
  fruits = []
  missiles = []
  clocks = []

  bottom = null
  boolBottom = false
  boolUseBottom = false
  countBottom = 0

  boostSpeed = 0
  
  countTimer = 0

  boolAddFive = false
  countAddFive = 0
  
  distChangePlaneX = 20
  boolGameOver = false
  score = 0

  /** start timer */
  startTimer(59, $('#time'))
  
  /** start timer boots */
  timerBoost()
  
  /** sound */
  pauseSound('mainSound')
  if (bgRand == 0) playSound('arenaSound')
  else playSound('arenaV2Sound')

  /** start game!!!!!!!!! */
  scene.start()
}

const playCoins = () => {
  /** random map */
  let bgRand = Math.floor(Math.random()*2)

  /** create scene */
  scene = new Scene('main')
  scene.setSize(900, 700)
  
  /** create plane  */
  plane = new Sprite(scene, myff.currentPlane.imgLeft, 150, 100)
  plane.setPosition(scene.width / 2, scene.height - 107)
  plane.setSpeed(0)
  
  /** create background in scene */
  background = new Sprite(scene,backgroundImgList[bgRand], 900 + 500, 700 + 300)
  background.setSpeed(0)

  /** create btoom */
  btoom = new Sprite(scene, './assets/img/bomb/btoom.png', 300, 300)

  /** reset value of var for init process */
  fruits = []
  missiles = []
  coinsList = []
  bottom = null
  boolBottom = false
  boolUseBottom = false
  countBottom = 0

  boostSpeed = 0

  distChangePlaneX = 20
  boolGameOver = false
  coinItem = 0
  bossHp = 50

  /** sound */
  pauseSound('mainSound')
  if (bgRand == 0) playSound('arenaSound')
  else playSound('arenaV2Sound')

  /** start game!!!!!!!!! */
  scene.start()
}

/** update will use on coins mode and survive mode  */
var update = () => {
  if (playMode == 1)
    updateCoins()
  else
    updateSurvive()
}

var updateSurvive = () => {
  scene.clear()
  // themeSong.play();
  background.update()
  spawnRand = Math.floor(Math.random() * 100) + 1
  if (spawnRand == 1 || spawnRand == 30 || spawnRand == 60) {
    spawnFruits()
  } else if (spawnRand == 15 || spawnRand == 45) {
    spawnMissiles()
  } else if (spawnRand == 31) {
    if (countTimer > 10) if (clocks.length < 2) spawnClocks()
  }

  if (!boolGameOver) {
    if (keysDown[K_LEFT]) {
      if (!boolUseBottom) plane.image.src = myff.currentPlane.imgLeft
      else plane.image.src = myff.currentPlane.imgLeftSpeed
      plane.changeXby(distChangePlaneX * -1)
    }

    if (keysDown[K_RIGHT]) {
      if (!boolUseBottom) plane.image.src = myff.currentPlane.imgRight
      else plane.image.src = myff.currentPlane.imgRightSpeed
      distChangePlaneX = Math.abs(distChangePlaneX)
      plane.changeXby(distChangePlaneX)
    }
    checkCollidesFruit()
    checkCollidesMissile()
    checkCollidesClock()
    if (boolBottom) {
      countBottom++
      if (countBottom < 20 * 3) {
        bottom.update()
      } else {
        countBottom = 0
        boolBottom = false
        bottom = null
      }
    }
    if (!boolUseBottom) if (bottom != null) checkCollidesBottom()
  }
  if (plane.x < 5) { plane.x = 5 }
  if (plane.x > 895) plane.x = 895
  plane.update()
  missiles.forEach((element, index) => {
    if (element.y > scene.height - 60) {
      missiles.splice(index, 1)
      index--
    } else {
      element.update()
    }
  })
  fruits.forEach((element, index) => {
    if (element.y > scene.height - 50) {
      fruits.splice(index, 1)
      index--
    } else {
      element.update()
    }
  })
  clocks.forEach((element, index) => {
    if (element.y > scene.height - 50) {
      clocks.splice(index, 1)
      index--
    } else {
      element.changeImgAngleBy(90)
      element.update()
    }
  })

  if (!boolGameOver) {
    if (boolAddFive) {
      countAddFive++
      if (countAddFive < 20) {
        showAddFiveSeconds()
      } else {
        boolAddFive = false
        countAddFive = 0
      }
    }
  }

  if (boolGameOver) {
    setMaxScore()
    surviveOver() 
  }
}

var updateCoins = () => {
  scene.clear()
  background.update()
  spawnRand = Math.floor(Math.random() * 100) + 1
  if (spawnRand == 1 || spawnRand == 30 || spawnRand == 60) {
    spawnFruits()
  } else if (spawnRand == 15 || spawnRand == 45) {
    spawnMissiles()
  } else if (spawnRand == 31) {
    spawnCoins()
  }

  if (!boolGameOver) {
    if (keysDown[K_LEFT]) {
      if (!boolUseBottom) plane.image.src = myff.currentPlane.imgLeft
      else plane.image.src = myff.currentPlane.imgLeftSpeed
      plane.changeXby(distChangePlaneX * -1)
    }

    if (keysDown[K_RIGHT]) {
      if (!boolUseBottom) plane.image.src = myff.currentPlane.imgRight
      else plane.image.src = myff.currentPlane.imgRightSpeed
      distChangePlaneX = Math.abs(distChangePlaneX)
      plane.changeXby(distChangePlaneX)
    }
    checkCollidesFruit()
    checkCollidesMissile()
    checkCollidesCoin()
    if (boolBottom) {
      countBottom++
      if (countBottom < 20 * 3) {
        bottom.update()
      } else {
        countBottom = 0
        boolBottom = false
        bottom = null
      }
    }
    if (!boolUseBottom) if (bottom != null) checkCollidesBottom()
  }
  if (plane.x < 5) {  plane.x = 5 }
  if (plane.x > 895) plane.x = 895
  plane.update()
  missiles.forEach((element, index) => {
    if (element.y > scene.height - 60) {
      missiles.splice(index, 1)
      index--
    } else {
      element.update()
    }
  })
  fruits.forEach((element, index) => {
    if (element.y > scene.height - 50) {
      fruits.splice(index, 1)
      index--
    } else {
      element.update()
    }
  })
  coinsList.forEach((element, index) => {
    if (element.y > scene.height - 50) {
      coinsList.splice(index, 1)
      index--
    } else {
      element.changeImgAngleBy(90)
      element.update()
    }
  })

  if (boolGameOver) {
    coinsOver() 
  }
}

var spawnFruits = () => {
  let fruit
  let randFruitType = Math.floor(Math.random() * 4) + 1
  let randFruitSize = Math.floor(Math.random() * 30) + 30
  let randFruitSpeed = Math.floor(Math.random() * 10) + 5
  let randX = Math.floor(Math.random() * 899) + 1
  switch (randFruitType) {
    case 1:
      fruit = new Sprite(
        scene,
        './assets/img/fruits/apple.png',
        randFruitSize,
        randFruitSize
      )
      break
    case 2:
      fruit = new Sprite(
        scene,
        './assets/img/fruits/grave.png',
        randFruitSize,
        randFruitSize
      )
      break
    case 3:
      fruit = new Sprite(
        scene,
        './assets/img/fruits/pair.png',
        randFruitSize,
        randFruitSize
      )
      break
    default:
      fruit = new Sprite(
        scene,
        './assets/img/fruits/orange.png',
        randFruitSize,
        randFruitSize
      )
      break
  }

  fruit.setSpeed(randFruitSpeed)
  fruit.setAngle(180)
  fruit.setPosition(randX, 0)

  fruits.push(fruit)
}

var spawnCoins = () => {
  
  let randCoinSize = Math.floor(Math.random() * 30) + 30
  let randCoinSpeed = Math.floor(Math.random() * 10) + 5
  let randX = Math.floor(Math.random() * 899) + 1
  let coin = new Sprite(
        scene,
        './assets/img/optional/coin.png',
        randCoinSize,
        randCoinSize
      )
 

  coin.setSpeed(randCoinSpeed)
  coin.setAngle(180)
  coin.setPosition(randX, 0)

  coinsList.push(coin)
}

var spawnMissiles = () => {
  let missile
  let randMissileSpeed = Math.floor(Math.random() * 5) + 20
  let randX = Math.floor(Math.random() * 899) + 1
  missile = new Sprite(scene, './assets/img/bomb/missile01.png', 80, 60)
  missile.setSpeed(randMissileSpeed + boostSpeed)
  missile.setAngle(180)
  missile.setPosition(randX, 0)
  missiles.push(missile)
}

var spawnClocks = () => {
  let clock
  let randClockSize = Math.floor(Math.random() * 30) + 30
  let randClockSpeed = Math.floor(Math.random() * 10) + 5
  let randX = Math.floor(Math.random() * 899) + 1
  let randImgAngle = Math.floor(Math.random() * 4) + 1
  clock = new Sprite(
    scene,
    './assets/img/clock/clock.png',
    randClockSize,
    randClockSize
  )
  clock.setSpeed(randClockSpeed + boostSpeed)
  clock.setMoveAngle(180)
  switch (randImgAngle) {
    case 1:
      clock.setImgAngle(90)
      break
    case 2:
      clock.setImgAngle(180)
      break
    case 3:
      clock.setImgAngle(270)
      break
    default:
      clock.setImgAngle(360)
  }
  clock.setPosition(randX, 0)
  clocks.push(clock)
}

var spawnBottoms = () => {
  let randX = Math.floor(Math.random() * 899) + 1
  bottom = new Sprite(scene, './assets/img/bottom/speedBottom.png', 70, 70)
  bottom.setPosition(randX, scene.height - 107)
  bottom.setSpeed(0)
}

var checkCollidesFruit = () => {
  fruits.forEach((element, index) => {
    if (plane.collidesWith(element)) {
      playSound('collectItemSound')
      fruits.splice(index, 1)
      index--
      if (playMode == 1) {
        bossHp--
        if (bossHp < (50 * 30 / 100)) {
          $('#bossBox').css('background-image', 'url("./assets/img/optional/bossBoxLess.png")')
        }
        $('#bossHp').text(bossHp)
        if (bossHp == 0) {
          boolGameOver = true
        }
      } else {
        score++
        $('#score').text(score)
      }

    }
  })
}

var checkCollidesCoin = () => {
  coinsList.forEach((element, index) => {
    if (plane.collidesWith(element)) {
      playSound('collectItemSound')
      coinsList.splice(index, 1)
      index--
      coinItem++
      $('#coin').text(coinItem)
    }
  })
}

var checkCollidesClock = () => {
  clocks.forEach((element, index) => {
    if (plane.collidesWith(element)) {
      clocks.splice(index, 1)
      index--
      timer += 5
      boolAddFive = true
    }
  })
}

var checkCollidesMissile = () => {
  missiles.forEach((element, index) => {
    if (plane.collidesWith(element)) {
      boolGameOver = true
    }
  })
}

var checkCollidesBottom = () => {
  if (plane.collidesWith(bottom)) {
    boolBottom = false
    distChangePlaneX = 50
    boolUseBottom = true
    let count = 0
    bottomIntervalID = setInterval(() => {
      count++
      if (count == 3) {
        distChangePlaneX = 20
        boolUseBottom = false
        bottom = null
        clearInterval(bottomIntervalID)
      }
    }, 1000)
  }
}

var startTimer = (duration, display) => {
  timer = duration,
       minutes, seconds
  intervalID = setInterval(function () {
    minutes = parseInt(timer / 60, 10)
    seconds = parseInt(timer % 60, 10)

    minutes = minutes < 10 ? '0' + minutes : minutes
    seconds = seconds < 10 ? '0' + seconds : seconds

    display.text(minutes + ':' + seconds)
    if (countTimer % 15 == 0) {
      spawnBottoms()
      boolBottom = true
    }

    if (--timer < 0) {
      boolGameOver = true
    }
  }, 1000)
}

var timerBoost = () => {
  boostIntervalID = setInterval(function () {
    countTimer++
    if (countTimer > 60) {
      if (boostSpeed != 30) boostSpeed++
    } else if (countTimer > 45 && boostSpeed == 5) {
      boostSpeed = 10
    } else if (countTimer > 30 && boostSpeed == 0) {
      boostSpeed = 5
    }
  }, 1000)
}

var surviveOver = () => {
  showGameOver()
  clearInterval(intervalID)
  clearInterval(boostIntervalID)
  score = 0
  myff.timePlaySurvive = myff.timePlaySurvive + 1
  saveMyff()
}

var coinsOver = () => {
  showGameOver()
  clearInterval(boostIntervalID)
  if (bossHp == 0) myff.userCoin += coinItem
  else myff.userCoin += ( coinItem - (Math.ceil(coinItem/2)) )
  coinItem = 0
  bossHp = 50
  saveMyff()
}


var showGameOver = () => {
  if (playMode == 2) {
    btoom.setPosition(plane.x, plane.y)
    btoom.update()
    scene.stop()
    toEndSurvive()
  } else {
    if (bossHp != 0) {
      btoom.setPosition(plane.x, plane.y)
      btoom.update()
    }
    scene.stop()
    toEndCoins()
  }
  
  
}

var setMaxScore = () => {
  if (score > myff.maxScore) myff.maxScore = score
}

var showAddFiveSeconds = () => {
  five = new Sprite(scene, './assets/img/clock/addFiveSecond.png', 40, 40)
  five.setPosition(scene.width - 50, 0 + 75)
  five.setSpeed(0)
  five.update()
}
