var myff = {}
var playMode = 0 /** 1 for coins mode, 2 for survive mode, 0 for exit play modes  */

/** localStorage */
if (window.localStorage.getItem('myff') == null) {
  initMyff()
  saveMyff()
} else {
  myff = { ...JSON.parse(window.localStorage.getItem('myff')) }
}